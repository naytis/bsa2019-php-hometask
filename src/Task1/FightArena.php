<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        if (count($this->fighters) === 0) {
            throw new \OutOfRangeException("Arena is empty");
        }

        return array_reduce(
            $this->fighters,
            function (Fighter $fighterA, Fighter $fighterB) {
                return $fighterA->getAttack() > $fighterB->getAttack() ? $fighterA : $fighterB;
            },
            $this->fighters[0]
        );
    }

    public function mostHealthy(): Fighter
    {
        if (count($this->fighters) === 0) {
            throw new \OutOfRangeException("Arena is empty");
        }

        return array_reduce(
            $this->fighters,
            function (Fighter $fighterA, Fighter $fighterB) {
                return $fighterA->getHealth() > $fighterB->getHealth() ? $fighterA : $fighterB;
            },
            $this->fighters[0]
        );
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
