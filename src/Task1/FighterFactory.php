<?php

declare(strict_types=1);

namespace App\Task1;

class FighterFactory
{
    private static $id = 0;

    public static function create(string $name, string $image): Fighter
    {
        self::$id++;
        return new Fighter(self::$id, $name, rand(0, 100), rand(0, 100), $image);
    }
}
