<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\FightArena;
use App\Task1\FighterFactory;
use App\Task3\FightArenaHtmlPresenter;

$arena = new FightArena();
$presenter = new FightArenaHtmlPresenter();

$arena->add(FighterFactory::create('Ryu', 'https://bit.ly/2E5Pouh'));
$arena->add(FighterFactory::create('Chun-Li', 'https://bit.ly/2Vie3lf'));
$arena->add(FighterFactory::create('Ken Masters', 'https://bit.ly/2VZ2tQd'));

$presentation = $presenter->present($arena);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
        crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <title>Built-in Web Server</title>
</head>
<body>
  <div class="container">
    <h1 class="mt-5 mb-5 text-center">Fighters</h1>
    <?php echo $presentation; ?>
  </div>
</body>
</html>
