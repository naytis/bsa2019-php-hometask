<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        if (count($arena->all()) === 0) {
            return "
              <p class='text-muted text-center pt-5'>
                There is no fighters yet and we dont know why..
              </p>";
        }

        $presentation = "<div class='row justify-content-around'>";
        foreach ($arena->all() as $fighter) {
            $presentation .= $this->createFighterCard($fighter);
        }
        $presentation .= "</div>";

        return $presentation;
    }

    /**
     * @param Fighter $fighter
     * @return string
     * @see https://getbootstrap.com/docs/4.3/components/card/
     */
    private function createFighterCard(Fighter $fighter): string
    {
        $name = $fighter->getName();
        $attack = $fighter->getAttack();
        $health = $fighter->getHealth();
        $image = $fighter->getImage();

        return "
          <div class='col-lg-3 col-md-4 col-7 mt-2 mb-2'>
            <div class='card text-center' aria-label='{$name}: {$attack}, {$health}'>
              <img src=\"{$image}\">
              <div class='card-body'>
                <h5 class='card-title'>{$name}</h5>
                <p class='card-text'>
                  &#x1F4AA; <strong>Attack</strong>: {$attack} <br>
                  &#x1F9E1; <strong>Health</strong>: {$health}
                </p>
              </div>
            </div>
          </div>
        ";
    }
}
